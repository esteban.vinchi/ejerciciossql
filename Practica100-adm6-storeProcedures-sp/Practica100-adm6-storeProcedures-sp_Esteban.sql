use cine_Esteban;

-- sin el set identity_insert peliculas on da error y aplica el rollback

set identity_insert peliculas on

begin try
	begin transaction
		insert into generos values('Ciencia Ficción');
		insert into generos values('Documental');
		insert into generos values('Policial');
		insert into peliculas (PEL_ID, GEN_ID, PEL_DESCRIPCION) values (50, 5, 'Esperando la Carroza');
	commit transaction
end try
begin catch
	rollback;
	print 'Ocurrio un error';
	throw
end catch
set identity_insert peliculas off


select * from peliculas
select * from generos

use cine_Esteban;
BACKUP DATABASE cine_Esteban
to disk = 'D:\Curso SQL\Practicas en clase\cine_Esteban_25102022.bak'
WITH INIT, COMPRESSION;


/*
Realizar un stored procedure llamado sp_modificarImdb que primita ingresar la pel_id y el puntaje de imdb, 
luego con esos datos, modificar la tabla película con los datos ingresados
*/



use cine_Esteban;
drop PROCEDURE sp_modificarImdb;

use cine_Esteban;
go
CREATE PROCEDURE sp_modificarImdb(@PEL_ID INT, @PEL_IMBD FLOAT )
	
	as
		update peliculas
		set PEL_IMBD = @PEL_IMBD
		where PEL_ID = @PEL_ID;		
		

/*
Ejecutar el sp anterior asignan puntaje a  7.6 a  la película “la pistola desnuda”
*/

use cine_Esteban;

execute sp_modificarImdb 1,7.6;

select * from peliculas



/*
Modificar el sp de manera de manera de verificar que el parámetro de imdb se encuentre en tre 0 y 10 y de no darse esta condición se deberá informar el texto 'los valores de imdb deben tener valores entre 0 y 10'
*/

--se agrega un IF y un else, importante hacer primero un drop procedure para que la borre y la vuelva a crear

use cine_Esteban;
drop PROCEDURE sp_modificarImdb;

use cine_Esteban;
go
CREATE PROCEDURE sp_modificarImdb(@PEL_ID INT, @PEL_IMBD FLOAT )
	
	as
		if @PEL_IMBD >0 and @PEL_IMBD  <11
			update peliculas
			set PEL_IMBD = @PEL_IMBD
			where PEL_ID = @PEL_ID;		

		else
			print 'los valores de imdb deben tener valores entre 0 y 10'



/*
Correr el sp de manera de realizar las siguientes siguientes asignaciones
 la pistola desnuda 7.6
 gladirador 8.5
 Titanic 7.9
 Rambito y Rambon 5.1
 la llamda 7.1
 la profecia 7.5
 los bañeros mas locos del mjndo 6.5
 el resplandor 8.4
 batman 7.9
 miaion imposible 7.9
 encuentro explosiva 6.3
 el sr y la sra smith 6.5
 troya 3.9
 la isla siniestra 8.2
 */

use cine_Esteban;

	execute sp_modificarImdb 1, 7.6; 
	execute sp_modificarImdb 2, 8.5;
	execute sp_modificarImdb 3, 7.9;
	execute sp_modificarImdb 4, 5.1;
	execute sp_modificarImdb 5, 7.1;
	execute sp_modificarImdb 6, 7.5;
	execute sp_modificarImdb 7, 6.5;
	execute sp_modificarImdb 8, 8.4;
	execute sp_modificarImdb 9, 7.9;
	execute sp_modificarImdb 10, 7.9;
	execute sp_modificarImdb 11, 6.3;
	execute sp_modificarImdb 12, 6.5;
	execute sp_modificarImdb 13, 3.9;
	execute sp_modificarImdb 14, 8.2;

select * from peliculas



/*
Llamar a l mismo sp en, pero en esta oportunidad enviando un 11 como  imdb y -3, copiar los mensajes recibidos.
*/

use cine_Esteban;

	execute sp_modificarImdb 1, 11; 
	execute sp_modificarImdb 1, -3; 


/*
Realizar un sp de nominado sp_agregarPelicula, el cual tenga los siguientes parámetros.
	a. gen_id
	b. pel_desc
	c. pel_imdb
Se deberá validar el  imdb teniendo en cuanta que el rango debe ser entre 0 y 10 para el imdb  informando el error 'los valores de imdb deben deben tener valores entre 0 y 10'.
También se debe agregar una estructura try catch en donde informe en catch el error 'Se introdujo un genero inexistente'
*/

use cine_Esteban;
go
CREATE PROCEDURE sp_agregarPelicula(@GEN_ID INT, @PEL_DESC varchar(50), @PEL_IMBD FLOAT )
	
	as
		if @PEL_IMBD >0 and @PEL_IMBD  <11
		
			begin TRY
				insert into peliculas (GEN_ID, PEL_DESCRIPCION, PEL_IMBD)
				values (@GEN_ID, @PEL_DESC, @PEL_IMBD);
			end TRY

			begin catch
				THROW
				print 'Se introdujo un genero inexistente'
			end catch
		
		else
			print 'los valores de imdb deben tener valores entre 0 y 10'


/*
Ejecutar el stored procedure agregando las siguientes películas
	a. Esperando la carroza 6.5 de imdb, comedia
	b. Top gun -6.9 de imdb, accion
	c. jurasic park 8.2 de imdb, accioin
*/


use cine_Esteban

execute sp_agregarPelicula  5, 'Esperando la carroza', 6.5
execute sp_agregarPelicula  3, 'Top gun', 6.9
execute sp_agregarPelicula  3, 'Jurasik Parck', 8.2

select * from peliculas

/*
Llamar al sp y probar con un imbd erróneo y con una gen_id erróneo.
a. imdb 12
b. gen_id = 50
*/

use cine_Esteban

execute sp_agregarPelicula  50, 'Peli prueba erronea', 12




/*
Realizar un sp de nominado sp_modificarPelicula, el cual tenga los siguientes parámetros.
	a. pel_id
b. gen_id
	c. pel_desc
	d. pel_imdb
Se deberá validar el  imdb teniendo en cuanta que el rango debe ser entre 0 y 10 para el imdb  informando el error 'los valores de imdb deben deben tener valores entre 0 y 10'.
También se debe agregar una estructura try catch en donde informe en catch el error 'Se introdujo un genero inexistente'
*/


use cine_Esteban;
go
CREATE PROCEDURE sp_modificarPelicula(@PEL_ID int, @GEN_ID INT, @PEL_DESC varchar(50), @PEL_IMBD FLOAT )
	
	as
		if @PEL_IMBD >0 and @PEL_IMBD  <11
		
			begin TRY
				update peliculas 
					set GEN_ID			=	@GEN_ID		, 
						PEL_DESCRIPCION	=	@PEL_DESC	, 
						PEL_IMBD		=	@PEL_IMBD
					where PEL_ID = @PEL_ID
			end TRY

			begin catch
				THROW
				print 'Se introdujo un genero inexistente'
			end catch
		
		else
			print 'los valores de imdb deben tener valores entre 0 y 10'


/*
Modificar la pistola desnuda colocando el texto en mayúscula verificar que el cambio se realizó.
*/

execute sp_modificarPelicula  1,5,'LA PISTOLA DESNUDA', 7.6


select * from peliculas

/*
Realizar un sp llamado sp_getPeliculasByGenero, en donde recibe por parámetro el genero y devuelve una lista de las películas 
indiando la descripción del genero  con los campos GEN_DESCRIPTION, PEL_ID, PEL_DESCRIPCION, PEL_IMBD que pertenecen a ese 
género, para lo cual se deberá realizar un inner join
*/


use cine_Esteban;

go
CREATE PROCEDURE sp_getPeliculasByGenero(@GEN_ID INT)
	
	as
		select GEN_DESCRIPCION ,PEL_ID, PEL_DESCRIPCION, PEL_IMBD
		from peliculas as pel inner join generos as gen
		on pel.GEN_ID = gen.GEN_ID
		where pel.GEN_ID = @GEN_ID



/*
Ejecutar el sp para los géneros disponibles
*/


use cine_Esteban;

exec sp_getPeliculasByGenero 1
exec sp_getPeliculasByGenero 2
exec sp_getPeliculasByGenero 3


/*
Utilizar la función with result sets para modificar los encabezados del resultado de la consulta cambiando los nombres a 
[Genero] varchar(50),[Codigo] int,[nombre de película] varchar(50),[puntaje] float.
*/

use cine_Esteban;

exec sp_getPeliculasByGenero 3

with result sets
	(([Genero] varchar(50),	[Codigo] int, [nombre de película] varchar(50),	[puntaje] float	))

