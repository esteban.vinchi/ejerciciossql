use sae_Esteban;

create table audit_Provincias(
prov_id int null,
user_name char(16) null,
date datetime null,
prov_nombre_old varchar(200) null,
prov_nombre_new varchar(200) null
);

create trigger modif_provinciaDesc_Trigger
on provincias AFTER UPDATE
AS IF UPDATE(PROV_NOMBRE)
BEGIN
DECLARE @PROV_NOMBRE_OLD varchar(200)
DECLARE @PROV_NOMBRE_NEW varchar(200)
DECLARE @PROV_ID INT
SELECT @PROV_NOMBRE_OLD = (SELECT PROV_NOMBRE FROM deleted)
SELECT @PROV_NOMBRE_NEW = (SELECT PROV_NOMBRE FROM inserted)
SELECT @PROV_ID = (SELECT PROV_ID FROM inserted)
insert into audit_Provincias values 
(@PROV_ID, USER_NAME(), GETDATE(), @PROV_NOMBRE_OLD, @PROV_NOMBRE_NEW)
END


select * from provincias

update provincias 
set prov_nombre ='Buenos Aires_modif'
where prov_id='1';

select * from audit_Provincias