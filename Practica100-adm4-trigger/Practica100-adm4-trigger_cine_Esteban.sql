use cine_Esteban;

create table generos(
GEN_ID integer NOT NULL PRIMARY KEY(GEN_ID) IDENTITY,
GEN_DESCRIPCION VARCHAR(50)
);
create table peliculas(
PEL_ID integer NOT NULL PRIMARY KEY(PEL_ID) IDENTITY,
GEN_ID integer NOT NULL,
PEL_DESCRIPCION VARCHAR(50),
constraint FK_GEN_PEL FOREIGN KEY (GEN_ID)  REFERENCES GENEROS(GEN_ID),
);
insert into generos (GEN_DESCRIPCION) values 
	('TERROR'),
	('DRAMA'),
	('ACCION'),
	('DOCUMENTAL'),
	('COMEDIA');


insert into peliculas (GEN_ID, PEL_DESCRIPCION) values
	(5,'La pistola desnuda'),
	(3,	'Gladiador'),
	(2,	'titanic'),
	(5,	'Rambito y Rambon'),
	(1,	'La llamada'),
	(1,	'la Profecia'),
	(5,	'Los baeros mas locos del mundo'),
	(1,	'El Esplandor'),
	(3,	'Batman'),
	(3,	'Mision Imposible'),
	(3,	'Encuentro Explosivo'),
	(3,	'Sr y Sra Smith'),
	(3,	'Troya'),
	(1,	'La Isla siniestra');


/*
hasta aca se generaron las tablas generos y peliculas con sus datos
*/


use cine_Esteban;

create table audit_Peliculas(
pel_id int null,
user_name char(16) null,
date datetime null,
pel_descripcion_old varchar(50) null,
pel_descripcion_new varchar(50) null
);

create trigger modif_pelDescipcion_trigger
on peliculas AFTER UPDATE
AS IF UPDATE(PEL_DESCRIPCION)
BEGIN
DECLARE @PEL_DESCRIPCION_OLD varchar(50)
DECLARE @PEL_DESCRIPCION_NEW varchar(50)
DECLARE @PEL_ID INT
SELECT @PEL_DESCRIPCION_OLD = (SELECT PEL_DESCRIPCION FROM deleted)
SELECT @PEL_DESCRIPCION_NEW = (SELECT PEL_DESCRIPCION FROM inserted)
SELECT @PEL_ID = (SELECT PEL_ID FROM inserted)
insert into audit_Peliculas values 
(@PEL_ID, USER_NAME(), GETDATE(), @PEL_DESCRIPCION_OLD, @PEL_DESCRIPCION_NEW)
END


select * from peliculas

update peliculas 
set PEL_DESCRIPCION ='Gladiador_modif'
where PEL_ID='2';

select * from audit_Peliculas