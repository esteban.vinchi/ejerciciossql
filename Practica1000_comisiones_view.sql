use sae_Esteban
go

CREATE VIEW comisiones_view
as Select com_turno, com_division, alu_nombre, alu_apellido, prof_nombre, prof_apellido
from profesores as PROF
inner join comisiones as COM on COM.PROF_ID=PROF.PROF_ID
inner join alumnos as ALU on COM.COM_ID=ALU.COM_ID

select * from comisiones_view