/*
Crear una función denominada fn_getCantidadDePeliculasPorGenero que recibe como parámetro un numero de genero
*/
use cine_Esteban;
go
create function getCantidadDePeliculasPorGenero(@gen_id int) 
	returns int

	begin
		DECLARE @Cantidad int

		select @Cantidad = count(*)
		from peliculas
		where GEN_ID = @gen_id

				
		return @Cantidad;
	end

/*
Probar que funciona llamándola para cada uno de los géneros.
*/

use cine_Esteban;
	print 'cantidad de peliculas de 1 son ' + convert(varchar, dbo.getCantidadDePeliculasPorGenero(1) );
	print 'cantidad de peliculas de 3 son ' + convert(varchar, dbo.getCantidadDePeliculasPorGenero(2) );
	print 'cantidad de peliculas de 3 son ' + convert(varchar, dbo.getCantidadDePeliculasPorGenero(3) );
	print 'cantidad de peliculas de 4 son ' + convert(varchar, dbo.getCantidadDePeliculasPorGenero(4) );
	print 'cantidad de peliculas de 5 son ' + convert(varchar, dbo.getCantidadDePeliculasPorGenero(5) );

/*
Realizar una función denominada fn_getDescripcionDeGenero que reciba el gen_id y devuelva la descripción
*/



use cine_Esteban;
go
create function fn_getDescripcionDeGenero(@gen_id int) 
	returns varchar(50)

	begin
		DECLARE @descripcion varchar(50);

		select @descripcion = GEN_DESCRIPCION
		from generos
		where GEN_ID = @gen_id

				
		return @descripcion;
	end


use cine_Esteban;
	print 'cantidad de peliculas de ' + dbo.fn_getDescripcionDeGenero(1) + ' son ' + convert(varchar, dbo.getCantidadDePeliculasPorGenero(1) );
	print 'cantidad de peliculas de ' + dbo.fn_getDescripcionDeGenero(2) + ' son ' + convert(varchar, dbo.getCantidadDePeliculasPorGenero(2) );
	print 'cantidad de peliculas de ' + dbo.fn_getDescripcionDeGenero(3) + ' son ' + convert(varchar, dbo.getCantidadDePeliculasPorGenero(3) );
	print 'cantidad de peliculas de ' + dbo.fn_getDescripcionDeGenero(4) + ' son ' + convert(varchar, dbo.getCantidadDePeliculasPorGenero(4) );
	print 'cantidad de peliculas de ' + dbo.fn_getDescripcionDeGenero(5) + ' son ' + convert(varchar, dbo.getCantidadDePeliculasPorGenero(5) );
