use cine_Esteban;

if (select count(*) 
		from peliculas)>5
		begin
			print 'son mas de 5 peliculas'
			print 'la parte positiva'
			print 'si es mas de una linea debo hacer un bloque'
		end
else 
	   print 'son mas de 5 peliculas'


/*
hay que agregar una columna
*/

use cine_Esteban;

alter table peliculas
	add  PEL_IMBD float NULL; 

alter table peliculas
	drop column PEL_IMBD;

alter table peliculas
  add constraint FK_PEL_GEN FOREIGN KEY(GEN_ID) REFERENCES GENEROS(GEN_ID);

select * from peliculas

/*
ahora se agrega un ciclo while de 10 vueltas
*/

use cine_Esteban;
declare @CONT int;
set @CONT = 0;
print 'CONTADOR=' + CONVERT(VARCHAR, @CONT);

while (@CONT<10)
 begin
	 set @CONT=@CONT +1;
	 print 'CONTADOR=' + CONVERT(VARCHAR, @CONT);
 end

 /*
 agregar un select sobre la tabla peliculas donde establezca un where con cada vuelta del contador
 */
 -- uso el contador para que traiga que pelicula hay en cada pel_id, hay 14 peliculas en la tabla eso va a traer hasta la 10, si quiero que muestre otra cantidad hay que modificar el contador---
 -- si me fijo en la tabla mensajes voy a ver que con el contador en 0 no trajo nada porque no existe pelicula con pel_id 0--


use cine_Esteban;

declare @CONT int;
set @CONT = 0;
print 'CONTADOR=' + CONVERT(VARCHAR, @CONT);

while (@CONT<10)
 begin
	 set @CONT=@CONT +1;
	 print 'CONTADOR=' + CONVERT(VARCHAR, @CONT)
	 select * from peliculas
	 where pel_id=@CONT
 end

 select * from peliculas


 /*
 Ampliando el código anterior, ahora definir una variable para tener la cantidad de peliculas y modificar el código para utilizarla
 */

--lo asigno con select --
--le agregue el print para que muestre la cantidad de peliculas--

use cine_Esteban;

declare @CONT int;
declare @CANT_PEL int;
set @CONT = 0;
select @CANT_PEL = COUNT (*) from peliculas;
print 'CONTADOR=' + CONVERT(VARCHAR, @CONT);
print 'CANTIDAD_PEL=' + CONVERT(VARCHAR, @CANT_PEL);

while (@CONT<10)
 begin
	 set @CONT=@CONT +1;
	 print 'CONTADOR=' + CONVERT(VARCHAR, @CONT)
	 select * from peliculas
	 where pel_id=@CONT
 end


/*
Modificar el código, definir una variable donde se guarde el gen_id, y mostrarlo a traves de un print
*/



use cine_Esteban;

declare @CONT int;
declare @CANT_PEL int;
declare @GEN_ID int;

set @CONT = 0;
select @CANT_PEL = COUNT (*) from peliculas;
print 'CONTADOR=' + CONVERT(VARCHAR, @CONT);
print 'CANTIDAD_PEL=' + CONVERT(VARCHAR, @CANT_PEL);

while (@CONT<10)
 begin
	 set @CONT=@CONT +1;
	 print 'CONTADOR=' + CONVERT(VARCHAR, @CONT)
	 select * from peliculas
	 where pel_id=@CONT
	 select @GEN_ID = GEN_ID from peliculas
	 where pel_id=@CONT
	 print 'GENERO_ID=' + CONVERT(VARCHAR, @GEN_ID)

	 select * from generos
	 where GEN_ID = @GEN_ID

 end



 /*Luego, de acuerdo al género, que guardo en una variable @GEN_DESC, escribo los siguientes textos--
 a.		 La comedia me hace muy feliz.
b.		 La acción tiene mucho movimiento.
c.		El drama me hace llorar.
d.		El terror me da mucho miedo.
*/


use cine_Esteban;

declare @CONT int;
declare @CANT_PEL int;
declare @GEN_ID int;
declare @GEN_DESC varchar(50);

set @CONT = 0;
select @CANT_PEL = COUNT (*) from peliculas;
print 'CONTADOR=' + CONVERT(VARCHAR, @CONT);
print 'CANTIDAD_PEL=' + CONVERT(VARCHAR, @CANT_PEL);

while (@CONT<10)
 begin
	 set @CONT=@CONT +1;
	 print 'CONTADOR=' + CONVERT(VARCHAR, @CONT)
	 select * from peliculas
	 where pel_id=@CONT
	 select @GEN_ID = GEN_ID from peliculas
	 where pel_id=@CONT
	 print 'GENERO_ID=' + CONVERT(VARCHAR, @GEN_ID)

	 select @GEN_DESC = GEN_DESCRIPCION from generos
	 where gen_id=@GEN_ID;
	 print 'GEN_DESC=' + upper (@GEN_DESC);
	 if upper (@GEN_DESC)='COMEDIA' print 'la comedia me hace muy feliz'
	 else
	 if upper (@GEN_DESC)='ACCION' print 'la accion tiene mucho movimiento'
	 else
	 if upper (@GEN_DESC)='DRAMA' print 'el drama me hace llorar'
	 else
	 if upper (@GEN_DESC)='TERROR' print 'el terror me da mucho miedo'


	 select * from generos
	 where GEN_ID = @GEN_ID

 end



 /*
A continuación, asigno el valor imdb de acuerdo al género y la siguiente tabla
a.	comedia 4.5
b.	accion 6.8
c.	drama 5.3
d.	terror 5.7 
*/

use cine_Esteban;

declare @CONT int;
declare @CANT_PEL int;
declare @GEN_ID int;
declare @GEN_DESC varchar(50);

set @CONT = 0;
select @CANT_PEL = COUNT (*) from peliculas;
print 'CONTADOR=' + CONVERT(VARCHAR, @CONT);
print 'CANTIDAD_PEL=' + CONVERT(VARCHAR, @CANT_PEL);

while (@CONT<10)
 begin
	 set @CONT=@CONT +1;
	 print 'CONTADOR=' + CONVERT(VARCHAR, @CONT)
	 select * from peliculas
	 where pel_id=@CONT
	 select @GEN_ID = GEN_ID from peliculas
	 where pel_id=@CONT
	 print 'GENERO_ID=' + CONVERT(VARCHAR, @GEN_ID)

	 select @GEN_DESC = GEN_DESCRIPCION from generos
	 where gen_id=@GEN_ID;
	 print 'GEN_DESC=' + upper (@GEN_DESC);

	 if upper (@GEN_DESC)='COMEDIA' 
		begin
			print 'la comedia me hace muy feliz'
			update peliculas
			set pel_imbd = 4.5
			where pel_id = @CONT;
		end

	 else
	 if upper (@GEN_DESC)='ACCION' 
		begin 
			print 'la accion tiene mucho movimiento'
			update peliculas
			set pel_imbd = 6.8
			where pel_id = @CONT;
		end

	 else
	 if upper (@GEN_DESC)='DRAMA' 
		begin 
			print 'el drama me hace llorar'
			update peliculas
			set pel_imbd = 5.3
			where pel_id = @CONT;
		end

	 else
	 if upper (@GEN_DESC)='TERROR' 
		begin 
			print 'el terror me da mucho miedo'
			update peliculas
			set pel_imbd = 5.7
			where pel_id = @CONT;
		end
/*
	 select * from generos
	 where GEN_ID = @GEN_ID
*/
 end

 /*
 asignar null a todos los pel_imbd 
 */


 use cine_Esteban;
 update peliculas
 set pel_imbd = NULL;

 select * from peliculas

 /*
 Realizar un inner join en donde se obtenga pel_id, pel_descripcion,  gen.gen_id, GEN_DESCRIPCION
 */


use cine_Esteban
select pel_id, pel_descripcion,  gen.gen_id, GEN_DESCRIPCION
from peliculas as pel inner join generos as gen
on pel.GEN_ID = gen.GEN_ID

/*
13-	Realizar un cursor y recorrerlo de manera de guardar cada uno de los campos (pel_id, pel_descripcion,  gen.gen_id, GEN_DESCRIPTION) en variables y sean mostrados a partir de un print
*/

use cine_Esteban;

--defino el cursor--

declare @GEN_ID int;
declare @GEN_DESC varchar(50);
declare @PEL_ID int;
declare @PEL_DESC varchar(50);
declare @P_CURSOR as CURSOR; 


SET @P_CURSOR = CURSOR FOR
	select pel_id, pel_descripcion,  gen.gen_id, GEN_DESCRIPCION
	from peliculas as pel inner join generos as gen
	on pel.GEN_ID= gen.GEN_ID;


open @P_CURSOR;

FETCH NEXT FROM @P_CURSOR INTO @PEL_ID, @PEL_DESC, @GEN_ID, @GEN_DESC
WHILE @@FETCH_STATUS=0
BEGIN
	PRINT 'PEL_ID=' + CONVERT(VARCHAR,@PEL_ID);
	PRINT 'PEL_DESC=' + CONVERT(VARCHAR,@PEL_DESC);
	PRINT 'GEN_ID=' + CONVERT(VARCHAR,@GEN_ID);
	PRINT 'GEN_DESC=' + CONVERT(VARCHAR,@GEN_DESC);
	PRINT '------------'

	FETCH NEXT FROM @P_CURSOR INTO @PEL_ID, @PEL_DESC, @GEN_ID, @GEN_DESC;
END
CLOSE @P_CURSOR;
DEALLOCATE @P_CURSOR;


/*
Utilizar ele ejercicio anterior agregando  los  updates modificando los imdb según lo  mostrados en el punto 10
*/


use cine_Esteban;

declare @GEN_ID int;
declare @GEN_DESC varchar(50);
declare @PEL_ID int;
declare @PEL_DESC varchar(50);
declare @P_CURSOR as CURSOR; 
declare @CONT int;



SET @P_CURSOR = CURSOR FOR
	select pel_id, pel_descripcion,  gen.gen_id, GEN_DESCRIPCION
	from peliculas as pel inner join generos as gen
	on pel.GEN_ID= gen.GEN_ID;


open @P_CURSOR;

FETCH NEXT FROM @P_CURSOR INTO @PEL_ID, @PEL_DESC, @GEN_ID, @GEN_DESC
WHILE @@FETCH_STATUS=0
BEGIN
	PRINT 'PEL_ID=' + CONVERT(VARCHAR,@PEL_ID);
	PRINT 'PEL_DESC=' + CONVERT(VARCHAR,@PEL_DESC);
	PRINT 'GEN_ID=' + CONVERT(VARCHAR,@GEN_ID);
	PRINT 'GEN_DESC=' + CONVERT(VARCHAR,@GEN_DESC);
	PRINT '------------'
	
	 if upper (@GEN_DESC)='COMEDIA' 
		begin
			print 'la comedia me hace muy feliz'
			update peliculas
			set pel_imbd = 4.5
			where pel_id = @CONT;
		end

	 else
	 if upper (@GEN_DESC)='ACCION' 
		begin 
			print 'la accion tiene mucho movimiento'
			update peliculas
			set pel_imbd = 6.8
			where pel_id = @CONT;
		end

	 else
	 if upper (@GEN_DESC)='DRAMA' 
		begin 
			print 'el drama me hace llorar'
			update peliculas
			set pel_imbd = 5.3
			where pel_id = @CONT;
		end

	 else
	 if upper (@GEN_DESC)='TERROR' 
		begin 
			print 'el terror me da mucho miedo'
			update peliculas
			set pel_imbd = 5.7
			where pel_id = @CONT;
		end

	FETCH NEXT FROM @P_CURSOR INTO @PEL_ID, @PEL_DESC, @GEN_ID, @GEN_DESC;
END
CLOSE @P_CURSOR;
DEALLOCATE @P_CURSOR;

/*
Hay que ingresar los sigientes generos y la peli Esperando la carroza’ con el gen_id = 50
a.	Ciencia ficción
b.	Documental
c.	Policial
*/


use cine_Esteban;

-- sin el set identity_insert peliculas on da error y aplica el rollback

set identity_insert peliculas on

begin try
	begin transaction
		insert into generos values('Ciencia Ficción');
		insert into generos values('Documental');
		insert into generos values('Policial');
		insert into peliculas (PEL_ID, GEN_ID, PEL_DESCRIPCION) values (50, 5, 'Esperando la Carroza');
	commit transaction
end try
begin catch
	rollback;
	print 'Ocurrio un error';
	throw
end catch
set identity_insert peliculas off


select * from peliculas
select * from generos