use sae_Esteban
go

CREATE VIEW nombreApellidoPartido_view
as Select alu_nombre, alu_apellido, part_nombre
from partidos as PART
inner join alumnos as ALU on ALU.PART_ID=PART.PART_ID

select * from nombreApellidoPartido_view