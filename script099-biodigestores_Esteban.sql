use bio_Esteban;

create table biodigestores(
bio_id int not null identity(1,1),
bio_nombre varchar(50),
bio_latitud varchar(50),
bio_longitud varchar(50),
constraint PK_BIO_ID primary key(bio_id)
);

create table tiposDeSensores(
tip_id int not null identity(1,1),
tip_Descripcion varchar(50),
tip_Unidad_Medida varchar(50),
constraint PK_TIP primary key(tip_id)
);

create table sensores(
sen_id int not null identity(1,1) primary key,
bio_id int not null,
tip_id int not null,
sen_marca varchar(50),
sen_modelo varchar(50),
constraint FK_BIO foreign key (bio_id) references biodigestores(bio_id),
constraint FK_TIP foreign key (tip_id) references tiposDeSensores(tip_id)
);

create table mediciones(
med_id int not null identity(1,1),
sen_id int not null,
med_Valor int,
med_Fecha_Hora varchar(50),
constraint PK_MED_ID primary key(med_id),
constraint FK_SEN_ID foreign key(sen_id) references sensores(sen_id)
);

create table personas(
per_id int not null identity(1,1),
per_Nombre varchar(50),
per_Apellido varchar(50),
per_Fecha varchar(50),
constraint PK_PER_ID primary key(per_id)
);

create table personas_biodigestores(
per_id int not null,
bio_id int not null,
constraint PK_COMPUESTA_PER_BIO primary key(per_id,bio_id),
constraint FK_PER_ID foreign key(per_id) references personas(per_id),
constraint FK_BIO_ID foreign key(bio_id) references biodigestores(bio_id)
);

/*
carga de datos en las tablas
*/

INSERT INTO biodigestores (bio_nombre,bio_latitud,bio_longitud)  VALUES
('Anakin','S34°39"12.38"','O58°37 11.1"'), 
('Obi Wan','S34°39"15.22"','O58°37"13.1"'),
('Darth Vader','S34°39"10.38"','O58°37"09.3"');

INSERT INTO tiposDeSensores (tip_Descripcion,tip_Unidad_Medida) VALUES
('Temperatura','Grados Centigrados'), 
('Humedad','%'),
('Presion','PSI');

INSERT INTO sensores (bio_id,tip_id,sen_marca,sen_modelo) VALUES
(1,1,'IST','AG OUT'), 
(1,2,'HUMIDITY','H1101'),
(1,3,'HONEYWELL','MLH'),
(2,2,'HUMIDITY','H1101'),
(2,3,'HONEYWELL','MLH'),
(3,1,'IST','AG OUT');

INSERT INTO personas (per_Nombre,per_Apellido,per_Fecha) VALUES
('Juan','PEREZ','13/2/1985'),
('Gabriel','CASAS','17/11/1972'),
('Susana','ROMERO','13/5/1990');

INSERT INTO personas_biodigestores (per_id,bio_id) VALUES
(1,1),
(1,2),
(1,3),
(2,1),
(2,3),
(3,3);

INSERT INTO mediciones (sen_id,med_Valor,med_Fecha_Hora) VALUES
(1,12.3,'11/10/2022  08:00:00'),
(1,12.5,'11/10/2022  08:05:00'),
(1,12.8,'11/10/2022  08:10:00'),
(1,13.1,'11/10/2022  08:15:00'),
(1,13.3,'11/10/2022  08:20:00'),
(1,12.0,'11/10/2022  08:25:00'),
(2,33.5,'11/10/2022  08:00:00'),
(2,43.2,'11/10/2022  08:05:00'),
(2,45.8,'11/10/2022  08:10:00'),
(2,50.2,'11/10/2022  08:15:00'),
(2,48.1,'11/10/2022  08:20:00'),
(2,49.7,'11/10/2022  08:25:00');


select * from sensores

backup database bio_Esteban
to disk = 'D:\Curso SQL\Practicas en clase\bio_Esteban.bak'
with init, compression

/*
55-	Modificar la tabla biodigestores de manera que el campo BIO_NOMBRE sea único
*/

alter table biodigestores
add constraint UC_BIO_NOMBRE 
unique (BIO_NOMBRE);


select * from biodigestores